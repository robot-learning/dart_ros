#ifndef DART_ROS_JOINT_INV_DYN_CONTROLLER_H_
#define DART_ROS_JOINT_INV_DYN_CONTROLLER_H_

#include <dart/dart.hpp>

class JointInvDynController
{
public:
  JointInvDynController(const dart::dynamics::SkeletonPtr &robot) : robot_(robot)
  {
    if (robot)
    {
      q_desired_ = robot->getPositions();
    }

    Kp_ = 1000.0;
    Kd_ = 50.0;
  }

  void setJointTorques();

  void setJointPositions(Eigen::VectorXd desired);

  void setDesiredJointPositions(Eigen::VectorXd desired);

protected:
  dart::dynamics::SkeletonPtr robot_;
  Eigen::VectorXd q_desired_;
  Eigen::VectorXd q_actual_;
  Eigen::VectorXd q_torque_;
  Eigen::VectorXd q_error_;
  Eigen::VectorXd dq_error_;
  Eigen::VectorXd dq_actual_;
  Eigen::VectorXd gravity_coriolis_;
  Eigen::MatrixXd mass_;
  double Kp_;
  double Kd_;
};

#endif
