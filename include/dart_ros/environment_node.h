#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include "environment.h"
#include <dart_ros/AddRobot.h>
#include <dart_ros/AddObject.h>
#include <dart_ros/EnvironmentState.h>
#include <geometry_msgs/Pose.h>
#include <ll4ma_teleop/OmniCommand.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/WrenchStamped.h>

typedef std::shared_ptr<visualization_msgs::Marker> MarkerPtr;

class EnvironmentNode
{
public:
  EnvironmentNode(const ros::NodeHandle nh,
                  const std::shared_ptr<Environment> &environment,
                  const int rate)
      : nh_(nh), environment_(environment), rate_(rate)
  {
    run_simulation_ = false;
    use_os_control_ = true;
    marker_id_ = 0;
  }

  void init();

  void runHeadless();

  void runSimWindow(int argc, char *argv[]);

  bool addRobot(dart_ros::AddRobot::Request &req,
                dart_ros::AddRobot::Response &resp);

  bool startSimulation(std_srvs::Trigger::Request &req,
                       std_srvs::Trigger::Response &resp);

  bool stopSimulation(std_srvs::Trigger::Request &req,
                      std_srvs::Trigger::Response &resp);

  bool addObject(dart_ros::AddObject::Request &req,
                 dart_ros::AddObject::Response &resp);

  bool resetDARTEnvironment(std_srvs::Trigger::Request &req,
                            std_srvs::Trigger::Response &resp);

  void publishEnvironmentState();

  void publishForceFeedback();

  void setDesiredRobotPose(ll4ma_teleop::OmniCommand msg);

  void setDesiredJointPositions(sensor_msgs::JointState desired);

  std::shared_ptr<Environment> getEnvironment();

  std::shared_ptr<dart::simulation::World> getWorld();

  std::shared_ptr<JointInvDynController> getController();

  dart::dynamics::SkeletonPtr getObject(const std::string &object_name);

  void setRenderedObjectPoses();

protected:
  ros::NodeHandle nh_;
  std::shared_ptr<Environment> environment_;
  bool use_os_control_;
  bool run_simulation_;
  ros::ServiceServer start_srv_;
  ros::ServiceServer add_robot_srv_;
  ros::ServiceServer add_object_srv_;
  ros::ServiceServer reset_env_srv_;
  ros::Publisher environment_state_pub_;
  ros::Publisher force_fb_pub_;
  ros::Subscriber omni_cmd_sub_;
  ros::Subscriber joint_sub_;
  ros::Rate rate_;
  dart_ros::EnvironmentState environment_state_;
  geometry_msgs::WrenchStamped end_effector_wrench_;
  std::map<std::string, MarkerPtr> object_map_;
  std::map<std::string, Eigen::Isometry3d> nominal_pose_map_;
  int marker_id_;
};