#include <dart/dart.hpp>
#include "joint_inv_dyn_controller.h"

class Environment
{
public:
  Environment(const dart::simulation::WorldPtr &world) : world_(world)
  {
    collision_detector_ = world->getConstraintSolver()->getCollisionDetector();
    // TODO setting this timestep because that seems to be roughly what allows
    // for a realtime factor of 1.0 when combined with a run rate of 300Hz,
    // which seems to be about the max rate for this kind of simulation. Can
    // try different collision checker or other modifications to see if we can
    // go faster, ideally it should go much faster.
    world_->setTimeStep(1.0 / 320.0); // Note this doesn't have to match rate
    // world_->setTimeStep(1.0 / 1000.0);
  }

  bool addRobot(const std::string &urdf_string,
                const std::string &package_name,
                const std::string &resource_path,
                const Eigen::VectorXd init_joint_positions);

  template <class JointType>
  bool addBox(const std::string &name, const double depth, const double width,
              const double height, const double mass, const Eigen::Isometry3d tf)
  {
    using namespace dart::dynamics;

    SkeletonPtr box = Skeleton::create(name);
    ShapePtr box_shape = std::make_shared<BoxShape>(Eigen::Vector3d(depth, width, height));

    typename JointType::Properties properties;
    properties.mName = name + "_joint";
    properties.mT_ParentBodyToJoint = tf;

    BodyNodePtr bn = box->createJointAndBodyNodePair<JointType>(nullptr, properties, BodyNode::AspectProperties(name)).second;

    bn->createShapeNodeWith<VisualAspect, CollisionAspect, DynamicsAspect>(box_shape);
    Inertia inertia;
    inertia.setMass(mass);
    inertia.setMoment(box_shape->computeInertia(mass));
    bn->setInertia(inertia);

    // TODO should probably check collision before adding but this wasn't
    // working correctly in the new version
    world_->addSkeleton(box);
  }

  bool checkCollision(const dart::dynamics::SkeletonPtr &object);

  void resetRobotJointPositions();

  dart::simulation::WorldPtr getWorld();

  std::shared_ptr<JointInvDynController> getController();

  dart::dynamics::SkeletonPtr getObject(const std::string &object_name);

  Eigen::Vector3d getForceFromContacts(const std::string &object_name);

  void step();

protected:
  dart::simulation::WorldPtr world_;
  std::shared_ptr<JointInvDynController> controller_;
  dart::collision::CollisionDetectorPtr collision_detector_;
  dart::collision::CollisionOption *collision_option_;
  dart::collision::CollisionResult *collision_result_;
  Eigen::VectorXd init_joint_positions_;
};