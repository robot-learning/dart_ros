# DART ROS

This package communicates the world state of a [DART](https://dartsim.github.io/) simulation and sends control commands via ROS topics. This is intended for running a DART simulation directly without utilizing Gazebo.

## Installation

There are two main dependencies:

1. [DART](https://dartsim.github.io/): the recommended setup for this is to use [this installation script](https://github.com/adamconkey/setup_scripts/blob/master/install_dartsim.sh). Otherwise, you will have to set `DART_DIR` differently in the CmakeLists.txt file in this repo. If that script doesn't work, contact Adam.
2. [ll4ma_teleop](https://bitbucket.org/robot-learning/ll4ma_teleop): follow the installation instructions on that README (just a catkin package with some internal dependencies).

## Usage

Currently there is just one demo environment. Launch this as

    roslaunch dart_ros run_simulation.launch
	
If you don't have the Phantom Omni setup on your machine, you can instead do

    roslaunch dart_ros run_simulation.launch use_omni:=false
	
In both cases you should see something like this:
![rviz environment](imgs/rviz_env.png)

If you have the Omni device setup, then you can click the buttons in the right panel to enable control and force feedback. Otherwise, you can't really do anything but look at the pretty environment.