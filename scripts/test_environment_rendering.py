#!/usr/bin/env python
import rospy
from dart_ros.srv import AddRobot, AddObject, AddObjectRequest
from std_srvs.srv import Trigger
from geometry_msgs.msg import Pose
from std_msgs.msg import ColorRGBA
from matplotlib import colors

if __name__ == '__main__':
    rospy.init_node("test_environment_rendering")

    add_robot = rospy.ServiceProxy("/dart_environment/add_robot", AddRobot)
    add_object = rospy.ServiceProxy("/dart_environment/add_object", AddObject)
    rospy.loginfo("Waiting for services...")
    rospy.wait_for_service("/dart_environment/add_object")
    rospy.wait_for_service("/dart_environment/add_robot")
    rospy.loginfo("Services found!")
    
    joint_names = ["lbr4_j{}".format(i) for i in range(7)]
    # init_joint_positions = [0.0, 0.0, 0.0, -1.57, 0.0, 1.57, 0.0]
    init_joint_positions = [
        0.3544750241067987, 0.8665801560030602, 0.28095257362211173,
        -0.9542183604791318, -0.21887403117667972, 1.3443958021378515,
        0.5892302617901322
    ]

    # TODO need to fix URDF path loading to be done from our description pkg
    import rospkg
    r = rospkg.RosPack()
    urdf_resource_path = r.get_path("dart_ros")
    
    add_robot(
        robot_description_name="/robot_description",
        urdf_resource_package="dart_ros",
        urdf_resource_path=urdf_resource_path,
        joint_names=joint_names,
        init_joint_positions=init_joint_positions)

    converter = colors.ColorConverter()
    # Color names:
    # http://matplotlib.org/mpl_examples/color/named_colors.hires.png

    # Add floor
    req = AddObjectRequest()
    floor_pose = Pose()
    floor_pose.position.x = 0.0
    floor_pose.position.y = 0.0
    floor_pose.position.z = -0.1
    floor_pose.orientation.x = 0
    floor_pose.orientation.y = 0
    floor_pose.orientation.z = 0
    floor_pose.orientation.w = 1
    color_values = converter.to_rgba(colors.cnames["darkblue"])
    color = ColorRGBA(*color_values)
    color.a = 0.8
    add_object(
        object_type=req.BOX,
        joint_type=req.FIXED_JOINT,
        object_name="floor",
        color=color,
        depth=5.0,
        width=5.0,
        height=0.2,
        mass=10.0,
        friction_coef=0.5,
        pose=floor_pose)

    # Add robot table
    req = AddObjectRequest()
    object_table_pose = Pose()
    object_table_pose.position.x = -0.8
    object_table_pose.position.y = 0.0
    object_table_pose.position.z = 0.295
    object_table_pose.orientation.x = 0
    object_table_pose.orientation.y = 0
    object_table_pose.orientation.z = 0
    object_table_pose.orientation.w = 1
    color_values = converter.to_rgba(colors.cnames["dimgrey"])
    color = ColorRGBA(*color_values)
    add_object(
        object_type=req.BOX,
        joint_type=req.FIXED_JOINT,
        object_name="object_table",
        color=color,
        depth=0.61,
        width=0.9125,
        height=0.59,
        mass=10.0,
        friction_coef=0.25,
        pose=object_table_pose)

    # Add object on table
    req = AddObjectRequest()
    object_pose = Pose()
    object_pose.position.x = -0.6
    object_pose.position.y = -0.4
    object_pose.position.z = 0.59 + 0.125  # height of table plus half the object size
    object_pose.orientation.x = 0
    object_pose.orientation.y = 0
    object_pose.orientation.z = 0
    object_pose.orientation.w = 1
    color_values = converter.to_rgba(colors.cnames["moccasin"])
    color = ColorRGBA(*color_values)
    add_object(
        object_type=req.BOX,
        joint_type=req.FREE_JOINT,
        object_name="object",
        color=color,
        depth=0.085,
        width=0.087,
        height=0.25,
        mass=0.684,
        friction_coef=0.5,
        pose=object_pose)

    start_simulation = rospy.ServiceProxy("/dart_environment/start_simulation",
                                          Trigger)
    start_simulation()
