#!/usr/bin/env python
import rospy
import tf2_ros
import numpy as np
from geometry_msgs.msg import WrenchStamped
from tf import transformations as tf

if __name__ == '__main__':
    rospy.init_node("force_transformer")

    orig_ft_topic = rospy.get_param("~orig_ft_topic")
    tf_ft_topic = rospy.get_param("~tf_ft_topic")
    base_frame = rospy.get_param("~base_frame", "world")
    target_frame = rospy.get_param("~target_frame", "push_ball_center")
    rate_val = rospy.get_param("~rate", 100)

    tf_buffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tf_buffer)
    rate = rospy.Rate(rate_val)
    wrench_pub = rospy.Publisher(tf_ft_topic, WrenchStamped, queue_size=1)

    force = np.ones(4)
    force_received = False
    tf_wrench = WrenchStamped()
    tf_wrench.header.frame_id = target_frame

    def ft_cb(msg):
        global force
        global force_received
        force_received = True
        force[0] = msg.wrench.force.x
        force[1] = msg.wrench.force.y
        force[2] = msg.wrench.force.z

    rospy.Subscriber(orig_ft_topic, WrenchStamped, ft_cb)

    while not rospy.is_shutdown():
        if force_received:
            try:
                tf_stmp = tf_buffer.lookup_transform(base_frame, target_frame,
                                                     rospy.Time())
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
                    tf2_ros.ExtrapolationException):
                rate.sleep()
                continue

            T = tf.quaternion_matrix(
                np.array([
                    tf_stmp.transform.rotation.x, tf_stmp.transform.rotation.y,
                    tf_stmp.transform.rotation.z, tf_stmp.transform.rotation.w
                ]))

            tf_force = np.dot(T, force)
            tf_wrench.wrench.force.x = tf_force[0]
            tf_wrench.wrench.force.y = tf_force[1]
            tf_wrench.wrench.force.z = tf_force[2]
            tf_wrench.header.stamp = rospy.Time.now()
            wrench_pub.publish(tf_wrench)

        rate.sleep()