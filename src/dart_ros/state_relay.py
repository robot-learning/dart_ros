#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseArray
from visualization_msgs.msg import MarkerArray
from dart_ros.msg import EnvironmentState


class EnvironmentStateRelay:
    def __init__(self):
        rate_val = rospy.get_param("~rate")
        robot_name = rospy.get_param("~robot_name")
        env_state_topic = rospy.get_param("~env_state_topic")

        self.rate = rospy.Rate(rate_val)
        rospy.Subscriber(env_state_topic, EnvironmentState,
                         self._set_env_state)

        self.joint_state_pub = rospy.Publisher(
            "/{}/joint_states".format(robot_name), JointState, queue_size=1)
        self.marker_pub = rospy.Publisher(
            "/object_markers", MarkerArray, queue_size=1)

        self.joint_state = None
        self.marker_array = None

    def run(self):
        rospy.loginfo("Ready to relay state.")
        while not rospy.is_shutdown():
            if self.joint_state:
                self.joint_state.header.stamp = rospy.Time.now()
                self.joint_state_pub.publish(self.joint_state)
            if self.marker_array:
                for marker in self.marker_array.markers:
                    marker.header.stamp = rospy.Time.now()
                self.marker_pub.publish(self.marker_array)
            self.rate.sleep()

    def _set_env_state(self, msg):
        if msg.robot_joint_state.position:
            self.joint_state = msg.robot_joint_state
            # TODO temporary need to populate velocity and effort and names
            self.joint_state.name = ["lbr4_j{}".format(i) for i in range(7)]
            self.joint_state.velocity = [0.0] * len(self.joint_state.position)
            self.joint_state.effort = self.joint_state.velocity
        if msg.marker_array.markers:
            self.marker_array = msg.marker_array


if __name__ == '__main__':
    rospy.init_node("environment_state_relay")
    relay = EnvironmentStateRelay()
    relay.run()