#include "joint_inv_dyn_controller.h"

using namespace dart::dynamics;

void JointInvDynController::setJointTorques()
{
    if (nullptr == robot_)
        return;

    q_actual_ = robot_->getPositions();
    dq_actual_ = robot_->getVelocities();
    q_actual_ += dq_actual_ * robot_->getTimeStep(); // Stable PD control

    q_error_ = q_desired_ - q_actual_;
    dq_error_ = -dq_actual_; // No desired velocity

    gravity_coriolis_ = robot_->getCoriolisAndGravityForces();
    mass_ = robot_->getMassMatrix();

    q_torque_ = mass_ * (Kp_ * q_error_ + Kd_ * dq_error_) + gravity_coriolis_;

    robot_->setForces(q_torque_);
}

void JointInvDynController::setJointPositions(Eigen::VectorXd desired)
{
    // This directly sets the joint positions ignoring any kind of accurate
    // dynamics in the simulation. It seems to ignore contacts
    robot_->setPositions(desired);
}

void JointInvDynController::setDesiredJointPositions(Eigen::VectorXd desired)
{
    q_desired_ = desired;
}
