#include "environment_node.h"
#include <visualization_msgs/MarkerArray.h>
#include <eigen_conversions/eigen_msg.h>
#include <dart/gui/gui.hpp>

using namespace dart::dynamics;
using namespace dart::simulation;

void EnvironmentNode::init()
{
    environment_state_pub_ = nh_.advertise<dart_ros::EnvironmentState>(
        "/dart_environment/state", 1);
    force_fb_pub_ = nh_.advertise<geometry_msgs::WrenchStamped>(
        "/dart_environment/force/raw", 1);

    joint_sub_ = nh_.subscribe("/lbr4/joint_cmd", 1,
                               &EnvironmentNode::setDesiredJointPositions, this);

    // Advertise the services this node offers
    start_srv_ = nh_.advertiseService("start_simulation",
                                      &EnvironmentNode::startSimulation, this);
    add_robot_srv_ = nh_.advertiseService("add_robot",
                                          &EnvironmentNode::addRobot, this);
    add_object_srv_ = nh_.advertiseService("add_object",
                                           &EnvironmentNode::addObject, this);
    reset_env_srv_ = nh_.advertiseService("reset_environment",
                                          &EnvironmentNode::resetDARTEnvironment, this);
}

void EnvironmentNode::runHeadless()
{
    ROS_INFO("DART environment is ready for building.");
    while (ros::ok())
    {
        ros::spinOnce();
        if (run_simulation_)
        {
            environment_->step();
            setRenderedObjectPoses();
            publishEnvironmentState();
            publishForceFeedback();
        }
        rate_.sleep();
    }
}

void EnvironmentNode::runSimWindow(int argc, char *argv[])
{
    class CustomWindow : public dart::gui::glut::SimWindow
    {
      public:
        CustomWindow(EnvironmentNode *env)
            : environment_node_(env)
        {
            setWorld(env->getWorld());
            controller_ = env->getController();
        }

        void timeStepping() override
        {
            if (controller_)
                controller_->setJointTorques();
            environment_node_->setRenderedObjectPoses();
            environment_node_->publishEnvironmentState();
            SimWindow::timeStepping();
        }

      protected:
        std::shared_ptr<EnvironmentNode> environment_node_;
        std::shared_ptr<JointInvDynController> controller_;
    };

    ROS_INFO("DART environment is ready for building.");

    while (ros::ok())
    {
        ros::spinOnce();
        if (run_simulation_)
        {
            CustomWindow window(this);
            glutInit(&argc, argv);
            window.initWindow(1280, 960, "DART Environment (DEBUG)");
            glutMainLoop();
        }
    }
}

bool EnvironmentNode::addRobot(dart_ros::AddRobot::Request &req,
                               dart_ros::AddRobot::Response &resp)
{
    std::string urdf_xml;
    nh_.getParam(req.robot_description_name, urdf_xml);
    Eigen::VectorXd init_positions(req.init_joint_positions.size());
    for (std::size_t i = 0; i < req.init_joint_positions.size(); ++i)
        init_positions[i] = req.init_joint_positions[i];
    resp.success = environment_->addRobot(urdf_xml,
                                          req.urdf_resource_package,
                                          req.urdf_resource_path,
                                          init_positions);
    environment_state_.robot_joint_state.name = req.joint_names;
    for (std::size_t i = 0; i < req.joint_names.size(); ++i)
    {
        environment_state_.robot_joint_state.position.push_back(0.0);
        environment_state_.robot_joint_state.velocity.push_back(0.0);
        environment_state_.robot_joint_state.effort.push_back(0.0);
    }

    // TODO hacking this for now, can make robust in service call later
    SkeletonPtr robot = getObject("lbr4");
    if (robot)
        robot->getBodyNode("push_stick_link")->setFrictionCoeff(0.5);

    ROS_INFO("Successfully added robot to DART environment.");
    return true;
}

bool EnvironmentNode::addObject(dart_ros::AddObject::Request &req,
                                dart_ros::AddObject::Response &resp)
{
    Eigen::Isometry3d eigen_pose;
    tf::poseMsgToEigen(req.pose, eigen_pose);

    if (req.BOX == req.object_type)
    {
        // Add object to simulation
        if (req.FIXED_JOINT == req.joint_type)
        {
            environment_->addBox<WeldJoint>(req.object_name, req.depth,
                                            req.width, req.height,
                                            req.mass, eigen_pose);
        }
        else if (req.FREE_JOINT == req.joint_type)
            environment_->addBox<FreeJoint>(req.object_name, req.depth,
                                            req.width, req.height,
                                            req.mass, eigen_pose);
        else
        {
            ROS_ERROR("Unknown joint type.");
            resp.success = false;
            return false;
        }

        // Store the nominal pose so environment can be reset
        nominal_pose_map_.insert(std::pair<std::string, Eigen::Isometry3d>(req.object_name, eigen_pose));

        // Set friction coefficient
        SkeletonPtr object = getObject(req.object_name);
        if (object)
        {
            object->getBodyNode(0)->setFrictionCoeff(req.friction_coef);
            object->getBodyNode(0)->setRestitutionCoeff(0.77);
        }

        // Add to local map for visualization
        MarkerPtr marker = dart::common::make_unique<visualization_msgs::Marker>();
        marker->id = ++marker_id_;
        marker->type = marker->CUBE;
        marker->action = marker->ADD;
        marker->color = req.color;
        marker->scale.x = req.depth;
        marker->scale.y = req.width;
        marker->scale.z = req.height;
        object_map_.insert(std::pair<std::string, MarkerPtr>(req.object_name, marker));
    }
    resp.success = true;
    return true;
}

bool EnvironmentNode::resetDARTEnvironment(std_srvs::Trigger::Request &req,
                                           std_srvs::Trigger::Response &resp)
{
    run_simulation_ = false;
    // Reset object poses
    for (auto &pair : nominal_pose_map_)
    {
        Eigen::Isometry3d new_pose(pair.second);
        Eigen::Vector6d vel(Eigen::Vector6d::Zero());
        Eigen::Vector6d acc(Eigen::Vector6d::Zero());

        FreeJoint *joint = dynamic_cast<FreeJoint *>(getObject(pair.first)->getJoint(0));
        if (joint)
            joint->setSpatialMotion(&new_pose, Frame::World(),
                                    &vel, Frame::World(), Frame::World(),
                                    &acc, Frame::World(), Frame::World());
    }

    // Reset robot joint positions
    environment_->resetRobotJointPositions();

    ROS_INFO("DART environment was reset to the nominal state.");
    resp.success = true;
    environment_->step();
    run_simulation_ = true;
    return true;
}

bool EnvironmentNode::startSimulation(std_srvs::Trigger::Request &req,
                                      std_srvs::Trigger::Response &resp)
{
    if (run_simulation_)
    {
        ROS_WARN("Simulation is already running. Doing nothing.");
        resp.success = false;
    }
    else if (getWorld()->getNumSkeletons() == 0)
    {
        ROS_WARN("World is empty, nothing to simulate yet.");
        run_simulation_ = false;
        resp.success = false;
    }
    else
    {
        run_simulation_ = true;
        ROS_INFO("Simulation started.");
        resp.success = true;
    }
    return true;
}

bool EnvironmentNode::stopSimulation(std_srvs::Trigger::Request &req,
                                     std_srvs::Trigger::Response &resp)
{
    if (!run_simulation_)
    {
        ROS_WARN("Simulation is not running. Doing nothing.");
        resp.success = false;
    }
    else
    {
        ROS_INFO("Simulation stopped.");
        resp.success = true;
    }
    return true;
}

void EnvironmentNode::setRenderedObjectPoses()
{
    for (auto &pair : object_map_)
    {
        Eigen::Isometry3d pose = getObject(pair.first)->getBodyNode(0)->getWorldTransform();
        pair.second->header.frame_id = "world";
        tf::poseEigenToMsg(pose, pair.second->pose);
    }
}

void EnvironmentNode::publishEnvironmentState()
{
    SkeletonPtr robot = environment_->getWorld()->getSkeleton("lbr4");
    if (robot)
    {
        Eigen::VectorXd positions = robot->getPositions();
        Eigen::VectorXd velocities = robot->getVelocities();
        for (std::size_t i = 0; i < positions.size(); ++i)
        {
            environment_state_.robot_joint_state.position[i] = positions[i];
            environment_state_.robot_joint_state.velocity[i] = velocities[i];
        }
    }

    environment_state_.marker_array = visualization_msgs::MarkerArray();
    for (auto &pair : object_map_)
        environment_state_.marker_array.markers.push_back(*pair.second);

    environment_state_pub_.publish(environment_state_);
}

void EnvironmentNode::publishForceFeedback()
{
    Eigen::Vector3d force = environment_->getForceFromContacts("push_stick_link");
    end_effector_wrench_.wrench.force.x = force[0];
    end_effector_wrench_.wrench.force.y = force[1];
    end_effector_wrench_.wrench.force.z = force[2];
    end_effector_wrench_.header.frame_id = "world";
    force_fb_pub_.publish(end_effector_wrench_);
}

void EnvironmentNode::setDesiredJointPositions(sensor_msgs::JointState desired)
{
    if (getController())
    {
        Eigen::VectorXd desired_positions(desired.position.size());
        for (std::size_t i = 0; i < desired.position.size(); ++i)
            desired_positions[i] = desired.position[i];
        getController()->setDesiredJointPositions(desired_positions);
    }
}

std::shared_ptr<Environment> EnvironmentNode::getEnvironment()
{
    return environment_;
}

WorldPtr EnvironmentNode::getWorld()
{
    return getEnvironment()->getWorld();
}

std::shared_ptr<JointInvDynController> EnvironmentNode::getController()
{
    return getEnvironment()->getController();
}

SkeletonPtr EnvironmentNode::getObject(const std::string &object_name)
{
    return getEnvironment()->getObject(object_name);
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "dart_ros_environment");
    ros::NodeHandle nh("~");
    WorldPtr world = std::make_shared<dart::simulation::World>();
    std::shared_ptr<Environment> environment(std::make_shared<Environment>(world));
    // TODO using rate 300 as that seems to be about as fast as it will go to
    // hit a realtime factor
    EnvironmentNode node(nh, environment, 500);
    // EnvironmentNode node(nh, environment, 1000);

    bool debug;
    nh.getParam("debug", debug);

    node.init();
    if (debug)
        node.runSimWindow(argc, argv);
    else
        node.runHeadless();
}
