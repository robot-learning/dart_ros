#!/usr/bin/env python
"""
This script is strictly to keep a 1000hZ publishing rate to render the omni
feedback. Both the virtual environment and force filter can lag under that
rate, which makes the omni feedback jittery. This ensures the omni feedback
always operates at a smooth rate.
"""

import rospy
from geometry_msgs.msg import WrenchStamped

if __name__ == '__main__':
    rospy.init_node("force_feedback_publisher")
    filtered_topic = rospy.get_param("~filtered_ft_topic")
    feedback_topic = rospy.get_param("~feedback_ft_topic")
    rate_val = rospy.get_param("~rate", 1000)
    rate = rospy.Rate(rate_val)
    wrench_stmp = None

    def filtered_cb(msg):
        global wrench_stmp
        wrench_stmp = msg

    feedback_pub = rospy.Publisher(feedback_topic, WrenchStamped, queue_size=1)
    rospy.Subscriber(filtered_topic, WrenchStamped, filtered_cb)

    while not rospy.is_shutdown():
        if wrench_stmp:
            feedback_pub.publish(wrench_stmp)
        rate.sleep()
