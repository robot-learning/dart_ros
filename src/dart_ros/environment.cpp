#include "environment.h"
#include "dart/utils/urdf/DartLoader.hpp"

using namespace dart::dynamics;
using namespace dart::simulation;
using namespace dart::collision;

bool Environment::addRobot(const std::string &urdf_string,
                           const std::string &package_name,
                           const std::string &resource_path,
                           const Eigen::VectorXd init_joint_positions)
{
    dart::utils::DartLoader loader;
    loader.addPackageDirectory(package_name, resource_path);
    SkeletonPtr robot = loader.parseSkeletonString(urdf_string, "");
    robot->enableSelfCollisionCheck();
    robot->setPositions(init_joint_positions);
    init_joint_positions_ = init_joint_positions; // Save for environment reset
    world_->addSkeleton(robot);

    controller_ = std::unique_ptr<JointInvDynController>(
        new JointInvDynController(robot));
    return true;
}

void Environment::resetRobotJointPositions()
{
    if (controller_)
    {
        controller_->setJointPositions(init_joint_positions_);
        controller_->setDesiredJointPositions(init_joint_positions_);
    }
}

WorldPtr Environment::getWorld()
{
    return world_;
}

std::shared_ptr<JointInvDynController> Environment::getController()
{
    return controller_;
}

SkeletonPtr Environment::getObject(const std::string &object_name)
{
    return getWorld()->getSkeleton(object_name);
}

Eigen::Vector3d Environment::getForceFromContacts(const std::string &object_name)
{
    // Compute forces being applied ON specified object. Will take sum
    // over all contacts being applied to object to get the net force.
    Eigen::Vector3d force = Eigen::Vector3d::Zero();
    SkeletonPtr robot = getObject("lbr4");
    BodyNodePtr end_effector = robot->getBodyNode("push_stick_link");
    const CollisionResult &result = world_->getLastCollisionResult();
    for (const auto &contact : result.getContacts())
    {
        for (const auto &shape : end_effector->getShapeNodesWith<CollisionAspect>())
        {
            if (shape == contact.collisionObject1->getShapeFrame())
                force += contact.force;
            else if (shape == contact.collisionObject2->getShapeFrame())
                force -= contact.force;
        }
    }

    return force;
}

void Environment::step()
{
    if (controller_)
        controller_->setJointTorques();
    world_->step();
}
